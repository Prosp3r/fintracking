package config

//Config type
type Config struct {
	config map[string]interface{}
}

//Get Fetch the config from file
func (c *Config) Get(serviceName string) (map[string]interface{}, error) {
	return nil, nil
}

//SetfromBytes Set data from file data type bytes
func (c *Config) SetfromBytes(data []byte) error {
	return nil
}
