A SIMPLE CENTRAL CONFIG TRACKER BY PROSPER
To serve as a central access point for all service and base config.
- Read config from YAML file
- Support base config
- Return service config in JSON format
- Config accessible as API call 
- Auto reload config every 20 seconds.

